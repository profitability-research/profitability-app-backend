const FalabellaAPIs = require('./api');

function Falabella(baseUrl) {
  /**
   * @type {FalabellaBaseUrl}
   */

  return new FalabellaAPIs(baseUrl);
}

module.exports = {
  Falabella
};