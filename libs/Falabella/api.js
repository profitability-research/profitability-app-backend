const strictUriEncode = require('strict-uri-encode');
const request = require('../Request');

module.exports = class Api {

    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    _sign(parameters) {
        const params = parameters
        const sortedParams = [];
    
        Object.keys(params)
          .sort()
          .forEach(name => {
            const value = params[name];
            const encoded = `${strictUriEncode(value)}`;
            sortedParams.push(encoded);
          });
        const concatenated = sortedParams.join('&');
    
        return concatenated;
      }


    /**
     * Execute Request
     * @param action
     * @param parameters
     * @returns {Promise}
     */

    call(method = 'get', service, parameters, data) {
        const signedParameters = this._sign(parameters);
        const options = {
            baseURL: ['init-product-shipping', 'get-product-shipping'].includes(service) ? this.baseUrl.concat(`${service}?${signedParameters}`) : this.baseUrl.concat(service),
            url: ['init-product-shipping', 'get-product-shipping'].includes(service) ? '' : `/cl?${parameters}`,
            method,
            timeout: '119000',
            data
        };
        return new Promise((resolve, reject) => request(options)
            .then(response => {
                const errResponse = response.data.ErrorResponse;
                return errResponse ? reject(errResponse.Head) : resolve(response);
            })
            .catch(err => reject(err)));
    };
};