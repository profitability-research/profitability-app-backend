function log(msg) {
  process.stdout.write(`${msg}\n`);
}

function interceptor(config) {
  const { DEBUG } = process.env;
  if (DEBUG === 'true') {
    if (config.data) log(`[Payload] ${JSON.stringify(config.data)}`);
  }
  return config;
}

module.exports = {
  interceptor
};
