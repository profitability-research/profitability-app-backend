const axios = require('axios');
const { interceptor } = require('./PayloadLogging');

const instance = axios.create();
instance.interceptors.request.use(interceptor);
instance.defaults.timeout = 30000;

module.exports = instance;