const moment = require('moment');

exports.getBestShippingDate = object => {
    return object.reduce((prev, curr) => {
        return (moment(prev.shippingDate).isSameOrAfter(curr.shippingDate)) ? prev : curr;
    });
};

exports.parseRFDate = date => {
    const splitDate = date.split('/');
    const day = splitDate[0].slice(-2)
    const month = splitDate[1].slice(0, 2);
    const year = moment().format('YYYY');
    return `${day}-${month}-${year}`;
};

exports.parseADDate = date => {
    const parsedADDate = moment(date).format('DD-MM-YYYY');
    return parsedADDate;
};
  