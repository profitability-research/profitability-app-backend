const productRipley = require('./ripley/searchProducto');
const shippingRipley = require('./ripley/shippingAvailability');
const productLinio = require('./linio/searchProducto');
const shippingLinio = require('./linio/shippingAvailability');
const searchProductMath = require('./utils/utils');

const controller = {};


controller.searchData = async (info) => {
   const data = {};
    const linio = await productLinio(info);
    const ripley = await productRipley(info);

    data.Linio = (searchProductMath(info.name.toUpperCase().replace(/["']/gi,'').trim(), info.brand.toUpperCase(), linio, "Linio"));
    data.Ripley = (searchProductMath(info.name.toUpperCase().replace(/["']/gi,'').trim(), info.brand.toUpperCase(), ripley, "Ripley"));
    return data;

  /*
    const data = await region("Región de Magallanes","Punta Arenas");
    console.log(data);
 /*
 console.log(info);
 const ripley = await productRipley(info);
 console.log(ripley);*/
}

controller.searchShipping = async (holding, sku, region, comuna) => {
    let shippingData=null;
    switch(holding){
        case "Linio":
            shippingData = await shippingLinio(sku, region, comuna);
            break;
        case "Mercadolibre":
            shippingData = await shippingLinio(sku, region, comuna);
            break;
        case "Ripley":
            shippingData = await shippingRipley(sku, region, comuna);
            break;
    }
    console.log(shippingData);
     return shippingData;
};

module.exports = controller;