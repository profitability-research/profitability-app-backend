const axios = require('axios');
const cheerio = require('cheerio');
const url = "https://www.linio.cl/search?scroll=&q=";

const searchData = async (info) => {
    const arrayRecord = [];
    await fetchData(url.concat(info.name)).then( (res) => {
    const html = res.data;
    const $ = cheerio.load(html);
    const statsTable = $('.catalogue-product');
    statsTable.each(function() {
        var product = {
            seller:"Linio",
            name:null,
            price:null,
            sku:null,
            brand:null,
            type:null,
            url:null,
            soldBy:null
        };
         let name = $(this).find("meta[itemprop='name']").attr("content").replace(/['"]/gi,'').trim();
         let priceBase = $(this).find('.price-main-md').text().replace(/[$.]/gi,'').trim();
         let pricePromotional = $(this).find('.price-promotional').text().replace(/[$.]/gi,'').trim();
         let sku = $(this).find("meta[itemprop='sku']").attr("content");
         let brand = $(this).find("meta[itemprop='brand']").attr("content");
         product.name = name.toUpperCase();
         product.price= pricePromotional==""?priceBase:pricePromotional;
         product.type = pricePromotional==""?"Normal":"CMR";
         product.sku=sku;
         product.brand=brand.toUpperCase();
         product.url="https://www.linio.cl".concat($(this).find("a").attr("href"));
         arrayRecord.push(product);
    });
})
 return arrayRecord;
}

async function fetchData(url){
    console.log("Crawling data Linio...")
    // make http call to url
    let response = await axios(url).catch((err) => console.log(err));

    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}


module.exports = searchData;