const axios = require('axios');
var moment = require('moment');
const urlBase = "https://www.linio.cl/api/catalog/shipping-estimate";
const FormData = require('form-data');

const shippingAvailability = async (sku, region, comuna) => {
    let shipping = {
        precio:null,
        date:null
    }
    await fetchShipping(urlBase, sku, region, comuna).then( (res) => {
        shipping.precio = res.data.title.replace(/[Envío$.]/gi,'').trim();
        let indice = res.data.estimatedDeliveryDate.indexOf("al");
        let date = res.data.estimatedDeliveryDate.substring(0, indice).trim();
        shipping.date=date;
})
 return shipping;
}

async function fetchShipping(urlBase, sku, region, comuna){
    console.log("getting shipping...")
    // make http call to url
    var bodyFormData = new FormData();
    bodyFormData.append('sku', sku);
    bodyFormData.append('region', region);
    bodyFormData.append('municipality', comuna);
    let response = await axios.post(urlBase, bodyFormData, {headers: bodyFormData.getHeaders()}).catch((err) => console.log(err));
    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}


module.exports = shippingAvailability;