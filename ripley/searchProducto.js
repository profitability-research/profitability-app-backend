const axios = require('axios');
const cheerio = require('cheerio');
const searchProductMath = require('../utils/utils');
const urlBase = "https://simple.ripley.cl/api/v2/search";

const searchData = async (info) => {
    let arrayRecord = [];
    
    await fetchData(urlBase, info).then( (res) => {
    let data = res.data.products;
    data.forEach( function(element) {
        let product = {
            seller:"Ripley",
            name:null,
            price:null,
            sku:null,
            brand:null,
            type:null,
            url:null,
            soldBy:null
        };
         product.name = element.name.toUpperCase().replace(/["']/gi,'').trim();
         product.price = price(element.prices);
         product.sku = element.selectedPartNumber;
         product.brand= element.manufacturer.toUpperCase();
         product.type= typeof element.prices.cardPrice !== "undefined"?"Normal":"Oferta";
         product.url=element.url;
         product.soldBy="";
         arrayRecord.push(product);
    })
    });
  
    return arrayRecord;
}

function price(prices){
    if (typeof prices.cardPrice !== "undefined") {
       return prices.cardPrice;
    }
    if (typeof prices.offerPrice !== "undefined") {
        return prices.offerPrice;
     }
     if (typeof prices.listPrice !== "undefined") {
        return prices.listPrice;
     }
     return 0;
}

function type(prices){
    if (typeof prices.cardPrice !== "undefined") {
       return "Card Price";
    }
    if (typeof prices.offerPrice !== "undefined") {
        return "Normal";
     }
     if (typeof prices.listPrice !== "undefined") {
        return "Normal";
     }
     return 0;
}


async function fetchData(urlBase,info){
    console.log("Api data Ripley...")
    // make http call to url
    let body = {
        "filters":[],
        "term":info.name,
        "perpage":24,
        "page":1,
        "sessionkey":"",
        "sort":"score"
    }

    let response = await axios.post(urlBase, body).catch((err) => console.log(err));
    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }

    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}


module.exports = searchData;