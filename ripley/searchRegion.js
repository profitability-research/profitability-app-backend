const axios = require('axios');
const urlBase = "https://simple.ripley.cl/api/v3/regions";

const searchRegion = async (region, comuna) => {
    let codeComune=null;
   
    await fetchLocation(urlBase).then((res) => {
        res.data.forEach( function(zona) {
            let compareZone = zona.hasOwnProperty("group")?zona.group:zona.name;
            if(region === compareZone){
                zona.children.forEach(function (comunaElement){
                    if(comunaElement.name===comuna){
                        codeComune = comunaElement.id;
                    }
                })
            }    
    })
    
})

 return codeComune;
}

async function fetchLocation(urlBase){
    console.log("getting zona shipping...")
    // make http call to url
    let response = await axios(urlBase).catch((err) => console.log(err));
    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}


module.exports = searchRegion;