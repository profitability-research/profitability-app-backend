const axios = require('axios');
var moment = require('moment');
const searchRegion = require('./searchRegion');
const urlBase = "https://simple.ripley.cl/api/v1/products/";

const shippingAvailability = async (sku, region, comuna) => {
    let shipping = {
        precio:null,
        date:null
    }
    console.log(comuna);
    console.log(region);
    var idLocation = await searchRegion(region, comuna);
    console.log(idLocation);
    await fetchShipping(urlBase.concat(sku).concat("/delivery-availability?subregion=").concat(idLocation)).then((res) => {
        res.data.schedule.forEach(function(shippingLocation) {
            var date = moment(shippingLocation.date).format('DD-MM-YYYY');
            shipping.date=date;
            shipping.precio=shippingLocation.options[0].cost;
            return true;
    })
})
 return shipping;
}

async function fetchShipping(urlBase){
    console.log("getting shipping ripley...")
    // make http call to url
    console.log(urlBase);
    let response = await axios(urlBase).catch((err) => console.log(err));
    if(response.status !== 200){
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}


module.exports = shippingAvailability;