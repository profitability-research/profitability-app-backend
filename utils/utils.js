const searchProductMatch = (falabellaProductName, brand, productList, bussines) => {
    
         productNull = {
            seller: bussines,
            name: null,
            price: null,
            sku: null,
            brand: null,
            type: null,
            url: null,
            soldBy: null
          }
        falabellaNamekeywords = falabellaProductName.split(" ")
    
        qualifiedProducts = []
    
        for (var key in productList) {
        var product = productList[key]
        if (brand == product.brand) {
            var d = new Levenshtein(falabellaProductName, product.name)
            if (d.distance == 0) {
            // Exact product is found and no further search is required
            qualifiedProducts.push({"distance": d.distance, "product": product, "wordMatched": falabellaNamekeywords.length, "is_exact_brand": true, "is_exact_product": true})
            break;
            }
            wordMatched = 0
            if (falabellaProductName.length > product.name.length){
            productNameWordList = product.name.split(" ")
                for(var key in productNameWordList) {
                if (falabellaProductName.toLowerCase().includes(productNameWordList[key].toLowerCase())) {
                    wordMatched++
                }
                }        
            } else {
                for(var key in falabellaNamekeywords) {
                if (product.name.toLowerCase().includes(falabellaNamekeywords[key].toLowerCase())) {
                    wordMatched++
                }
                }
            }
            qualifiedProducts.push({"distance": d.distance, "product": product, "wordMatched": wordMatched, "is_exact_brand": true})
        } else {
        var brandDistance = new Levenshtein(brand, product.brand)
        if (brandDistance.distance < 2) {
            var d = new Levenshtein(falabellaProductName, product.name)
            if (d.distance == 0) {
            // Exact product is found but in different brand and no further search is required
                qualifiedProducts.push({"distance": d.distance, "product": product, "wordMatched": falabellaNamekeywords.length, "is_exact_brand": false, "is_exact_product": true})
                break
            }
            wordMatched = 0
            if (falabellaProductName.length > product.name.length){
            productNameWordList = product.name.split(" ")
                for(var key in productNameWordList) {
                if (falabellaProductName.toLowerCase().includes(productNameWordList[key].toLowerCase())) {
                    wordMatched++
                }
                }        
            } else {
                for(var key in falabellaNamekeywords) {
                if (product.name.toLowerCase().includes(falabellaNamekeywords[key].toLowerCase())) {
                    wordMatched++
                }
                }
            }
            qualifiedProducts.push({"distance": d.distance, "product": product, "wordMatched": wordMatched, "is_exact_brand": false})
        }
        }
        }
    
        qualifiedProducts.sort((a, b) => getSortFunction("wordMatched", a, b));
     
        return qualifiedProducts.length===0?productNull:qualifiedProducts[0].product;
 }



function Levenshtein( str_m, str_n ) { var previous, current, matrix
    // Constructor
    matrix = this._matrix = []

    // Sanity checks
    if ( str_m == str_n )
        return this.distance = 0
    else if ( str_m == '' )
        return this.distance = str_n.length
    else if ( str_n == '' )
        return this.distance = str_m.length
    else {
        // Danger Will Robinson
        previous = [ 0 ]
        forEach( str_m, function( v, i ) { i++, previous[ i ] = i } )

        matrix[0] = previous
        forEach( str_n, function( n_val, n_idx ) {
        current = [ ++n_idx ]
        forEach( str_m, function( m_val, m_idx ) {
            m_idx++
            if ( str_m.charAt( m_idx - 1 ) == str_n.charAt( n_idx - 1 ) )
            current[ m_idx ] = previous[ m_idx - 1 ]
            else
            current[ m_idx ] = Math.min
                ( previous[ m_idx ]     + 1   // Deletion
                , current[  m_idx - 1 ] + 1   // Insertion
                , previous[ m_idx - 1 ] + 1   // Subtraction
                )
        })
        previous = current
        matrix[ matrix.length ] = previous
        })

        return this.distance = current[ current.length - 1 ]
    }
    }


    function forEach( array, fn ) { var i, length
    i = -1
    length = array.length
    while ( ++i < length )
        fn( array[ i ], i, array )
    }

    function getSortFunction(fieldName, item1, item2) {
    return item1[fieldName] > item2[fieldName];
    }


 
module.exports = searchProductMatch;