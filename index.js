require('dotenv').config();
const mappingLocalidades = require('./properties/mapping.json');
const { Falabella } = require('./libs/Falabella/helper');
const { getBestShippingDate, parseRFDate, parseADDate } = require('./commons/index');
const { searchData, searchShipping } = require('./main');
const _ = require('lodash');


const FALABELLA_BASE_URL = 'https://www.falabella.com/s/browse/v1/';

const FABELLA_REGIONS_BASE_URL = 'https://www.falabella.com/rest/model/falabella/rest/browse/BrowseActor/';

const api = new Falabella(FALABELLA_BASE_URL);
const apiRegions = new Falabella(FABELLA_REGIONS_BASE_URL);

const log = (msg) => {
    process.stdout.write(`${msg}\n`);
}

const exit = () => {
    process.exit(0);
}

const formatShippingObjectInfo = shippingMethods => {
    const shippingMethodsParsed = shippingMethods.map(shipping => {
        switch (shipping.label) {
            case 'Rango De Fechas':
                return {
                    shippingDate: parseRFDate(shipping.deliveryInfoDate),
                    shippingCost: shipping.deliverySlots[0]['allDayTimeSlot']['shippingCost']['purchasedPrice']
                };
            case 'Selecciona fecha y horario de despacho':
                return {
                    shippingDate: parseADDate(shipping.deliverySlots[0]['calendarDate']),
                    shippingCost: shipping.deliverySlots[0]['allDayTimeSlot']['shippingCost']['purchasedPrice']
                };
            default: return null;
        }
    });
    return getBestShippingDate(shippingMethodsParsed);
}

const getAvailabilityByComuna = async (region, comuna, sku) => {
    try {
        const shippinginfoByComuna = await apiRegions.call('get', 'get-product-shipping', { skuId: `{"skuId":${sku},"skuQuantity":1,"regionId":${region},"comunaId":${comuna}, "pageIndex":1}` }, {});
        if (shippinginfoByComuna.data.errors.length >= 1) {
            return Promise.resolve({
                shippingDate: 'No hay fecha disponible',
                shippingCost: undefined
            });

        }
        log('getting shipping for Falabella...');
        return formatShippingObjectInfo(shippinginfoByComuna.data.state.shippingMethods);
    } catch (err) {
        return Promise.resolve({
            shippingDate: 'Data no encontrada',
            shippingCost: undefined
        });
    }
};

const getAvailabilityByComunas = (regions, sku) => {
    return Promise.all(regions.map(region => {
        return getAvailabilityByComuna(region.id, region.comunas, sku);
    }));
}
// Incrementar precio del producto por no disponibilidad de otros vendedores
// Incrementar precio del producto por competencia con precio mayor
// Reducir precio del producto por precio menor de la competencia



const getOpportunities = (product, falabella, linio, ripley) => {
    return {};
};


const availabilityByRegions = async (region, product) => {
    try {
        const promises = await Promise.all(region.comunas.map(async comuna => {
            let Falabella = await getAvailabilityByComuna(region.id, comuna.id, product.Falabella.sku);
            let Linio = product.Linio.sku !== null ? await searchShipping('Linio', product.Linio.sku, region.name, comuna.name) : {};
            let Ripley = product.Ripley.sku !== null ? await searchShipping('Ripley', product.Ripley.sku, region.name, comuna.name) : {};
            return Object.assign({}, Falabella, Linio, Ripley);
        }));
        const x = promises;
        return promises;
    } catch (err) {
        log(err)
    }
};



const availabilityByLocations = async product => {
    try {
        const promise = await Promise.all(mappingLocalidades.map(region => availabilityByRegions(region, product)));
        const x = promise;
        return promise;
    } catch (err) {
        log(err);
    }
}


const resolveAvailability = async products => {
    try {
        const response = await Promise.all(products.map(product => availabilityByLocations(product)));
        return response;
    } catch (err) {
        log(err)
    }
};

const getProductAvailabilityByRegions = async products => {
    try {
        const response = resolveAvailability(products);
        return response;

    } catch (err) {
        log(err);
        throw err;
    }
};



const getAvailabilityByProduct = async productsDetails => {
    try {
        const products = await Promise.all(productsDetails.map(async product => {
            const Falabella = {
                seller: 'Falabella',
                brand: product.product.brand,
                name: product.product.displayName,
                price: Math.min(...product.product.prices.map(price => price.originalPrice.replace('.', ""))),
                sku: product.product.id,
                soldBy: null,
                type: 'Normal'
            };
            const data = {
                brand: product.product.brand,
                name: product.product.displayName,
                price: Math.min(...product.product.prices.map(price => price.originalPrice.replace('.', ""))),
                properties: product.product.topSpecifications
            };
            const regions = await apiRegions.call('get', 'init-product-shipping', { skuId: `{"skuId":"${product.product.id}","skuQuantity":"1"}` }, {});
            const sellersInfo = await searchData(data);
            const productList = Object.assign({}, { Falabella }, sellersInfo)
            return productList;
        }));
        const productAvailabilityByRegions = await getProductAvailabilityByRegions(products);

        //const productAvailabilityByRegions = await getProductAvailabilityByRegions(products);
        return productAvailabilityByRegions;
    } catch (err) {
        throw err;
    }
}

const main = async (input) => {
    try {
        const responseAPISearch = await api.call('get', 'search', `Ntt=${input.toString().concat('+de')}`, {});
        const products = responseAPISearch.data.data.results.map(product => `{"skuId":"${product.skuId}"}`);
        const products_chunk = [];
        products_chunk.push(products[0], products[1]);
        const responseAPIFetchItemDetails = await api.call('get', 'fetchItemDetails', `{"products":[${products_chunk.map(p => `${p}`).join(',')}]}`);
        const productsDetails = responseAPIFetchItemDetails.data.products;
        //const data = await getAvailabilityByComuna('13', '626', '14451740')
        const productsAvailability = await getAvailabilityByProduct(productsDetails);

        log(JSON.stringify(productsAvailability));
        exit();
    } catch (err) {
        log(err);
        exit();
    }
}

main('televisores');